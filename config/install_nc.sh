#!/bin/sh

TODAY=`date "+%d-%m-%Y"`
if [ -d /app/nextcloud ]
then
	echo "Removing old configuration, starting from scratchi\n"
	rm -rf /app/nextcloud
fi
if [ ! -f /app/latest.tar.bz2 ] || [ date -r /app/latest.tar.bz2 "+%d-%m-%Y" -ne $TODAY ]
then
	echo "You don't have the most recent version of nextcloud, downloading\n"
	wget https://download.nextcloud.com/server/daily/latest.tar.bz2
fi

cd /app
tar xvfj latest.tar.bz2

cd /app/nextcloud
echo "Installing Nextcloud via CLI"
php occ maintenance:install --database "mysql" --database-name "lamp" --database-user "lamp" --database-pass "lamp" --database-host "database" --admin-user "admin" --admin-pass "password"

cd /app/nextcloud/config/
echo "Access via web at the link provided by Lando with user \"admin\" and password \"password\""
sed -i s/localhost/nextcloud.lndo.site/g config.php
sed -i "3i\'debug\' => true, \
\'log_type\' => \'file\', \
\'logfile\' => \'/var/log/nextcloud.log\', \
\'logfilemode\' => 0640, \
\'loglevel\' => \'0\', \
\'logdateformat\' => \'F d, Y H:i:s\'," config.php

